$( document ).ready(function() {
    $('.show-icon-item.grid').on('click',function () {
        $('.photo-list').hide();
        $('.show-icon-item.list').removeClass('active');
        $('.show-icon-item.grid').addClass('active');
        $('.photo-grid-wrap').show();
    });
    $('.show-icon-item.list').on('click',function () {
        $('.photo-list').show();
        $('.show-icon-item.list').addClass('active');
        $('.photo-grid-wrap').hide();
        $('.show-icon-item.grid').removeClass('active');
    });
    if ($('.photo-list').find('.photo-item').length > 10) {
        $('.load-more1').on('click', function () {
            $('.photo-list .photo-item:nth-child(n+10)').slideToggle('').toggleClass('more');
            $(this).toggleClass('hide-more');
            if ($(this).hasClass('hide-more')) {
                $(this).html('HIDE');

            } else {
                $(this).html('LOAD MORE');
            }
        })
    } else {
        $('.load-more1').hide();
    }
    if ($('.photo-grid').find('.photo-item-grid').length > 10) {
        $('.load-more2').on('click', function () {
            $('.photo-grid .photo-item-grid:nth-child(n+10)').slideToggle('').toggleClass('more');
            $(this).toggleClass('hide-more');
            if ($(this).hasClass('hide-more')) {
                $(this).html('HIDE');

            } else {
                $(this).html('LOAD MORE');
            }
        })
    } else {
        $('.load-more2').hide();
    }

    $( function() {
        var dateFormat = "mm/dd/yy",
            from = $( "#datepicker-from" )
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    showOn: "button",
                    buttonImage: "img/cal-from.svg",
                    buttonImageOnly: true,
                    buttonText: "To"
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#datepicker-to" ).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                showOn: "button",
                buttonImage: "img/cal-from.svg",
                buttonImageOnly: true,
                buttonText: "To"
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
        $('.datepicker-from-close').on('click',function () {
            $( "#datepicker-from" ).datepicker('setDate', "");

        });
        $('.datepicker-to-close').on('click',function () {
            $( "#datepicker-to" ).datepicker('setDate', "");

        });

    });






});